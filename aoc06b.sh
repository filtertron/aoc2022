#!/bin/bash

mkdir -p junk

for i in `seq 1 4082`
do 
  dd if=aoc06.txt of=junk/$i bs=1 skip=$i count=14 2>/dev/null
done

for i in b c d f g h j l m n p q r s t v w z
do 
  for j in `seq 1 4082`
  do 
    grep -v "$i.*$i" junk/$j$oldi > junk/$j$i
  done
  oldi=$i
done

ls -tr junk/*z \
| xargs grep "." \
| head -1 \
| sed 's/.*\/\(.*\)z:.*/\1/' \
| xargs echo 14+ \
| bc

rm -rf junk
