#!/bin/bash

cat aoc01a.txt \
| perl -e 'my $total = 0; 
           while (<>) { 
             if (m/^$/) { 
               print "$total\n"; 
               $total=0;
             } 
             else { 
               $total += $_; 
             } 
           } print "$total\n";' \
| sort -nr \
| head -3 \
| sed 's/^/+ /' \
| xargs echo 0 \
| bc
