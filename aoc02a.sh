#!/bin/bash

declare -A RPS=(["A X"]=4 ["A Y"]=8 ["A Z"]=3 ["B X"]=1 ["B Y"]=5 ["B Z"]=9 ["C X"]=7 ["C Y"]=2 ["C Z"]=6)

TOTAL=0

cat aoc02.txt \
| while read L
  do
    TOTAL=$((TOTAL+${RPS[$L]}))
    echo $TOTAL
  done
