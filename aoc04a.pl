#!/usr/bin/perl -w

use strict;

my @l;
my $total = 0;

open F, "aoc04.txt" or die;
while (<F>) {
  s/,/-/;
  @l = split /-/;
  $total += 1
    if ((($l[0] <= $l[2]) and
         ($l[1] >= $l[3])) or
        (($l[0] >= $l[2]) and 
         ($l[1] <= $l[3])) );
}
print "$total\n";
