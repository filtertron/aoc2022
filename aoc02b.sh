#!/bin/bash

declare -A RPS=(["A X"]=3 ["A Y"]=4 ["A Z"]=8 ["B X"]=1 ["B Y"]=5 ["B Z"]=9 ["C X"]=2 ["C Y"]=6 ["C Z"]=7)

TOTAL=0

cat aoc02.txt \
| while read L
  do
    TOTAL=$((TOTAL+${RPS[$L]}))
    echo $TOTAL
  done
