#!/bin/bash


X=1
T=0

cat aoc10.txt \
| while read LINE
  do 
    I=${LINE/ */}
    V=${LINE/* /}
    if [ $I = noop ]
    then
      V=0
      C=$(($C+1))
      continue
    fi
    [ $I = addx ] && \
      C=$(($C+2))
    X=$(($X+$V))


    for STEP in 20 60 100 140 180 220
    do
      if [[ ( $C -eq $STEP ) || ( $C -eq $(($STEP+1)) ) ]]
      then
        T=$(($T + $STEP * $X))
        echo "$T"
      fi  
    done
  done
