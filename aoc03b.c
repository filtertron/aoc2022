#include <stdio.h>
#include <string.h>

#define LEN 64


int compare (char *l1, char *l2, char *l3) {

  int len1,
      len2, 
      len3,
      i, 
      j,
      k;

  len1 = strlen (l1);
  len2 = strlen (l2);
  len3 = strlen (l3);
  for (i = 0; i < len1; i++) {
    for (j = 0; j < len2; j++) {
      if (l1[i] == l2[j]) {
        for (k = 0; k < len3; k++) {
          if (l1[i] == l3[k]) {
            if ((l1[i] >= 'a') && (l1[i] <= 'z'))
              l1[i] = l1[i] - 96;
            if ((l1[i] >= 'A') && (l1[i] <= 'Z'))
              l1[i] = l1[i] - 38;
            return (int)l1[i];
          }
        }
      }
    }
  }
  printf ("Uh oh\n");
  return 0;
}


int main () {

  char l1 [LEN],
       l2 [LEN],
       l3 [LEN];
  FILE *f;
  int  res,
       sum = 0;

  f = fopen ("aoc03.txt" , "r");
  while (fgets (l1, LEN, f)) {
    fgets (l2, LEN, f);
    fgets (l3, LEN, f);
    res = compare (l1, l2, l3);
    sum += res;
  }
  printf ("%d\n", sum);
}
