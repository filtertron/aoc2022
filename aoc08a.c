#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>


int main () {

  int  f,
       i,
       j,
       max,
       sum = 0;
  char t[100][100],
       x[100][100];

  if ((f = open ("aoc08.txt" , O_RDONLY)) == -1)
    return 1;
  read (f, t, 10000);
  close (f);

  bzero (x, 10000);

  for (i=0; i<99; i++) {
    if (x[i][0] == 0) {
      x[i][0] = t[i][0];
      sum++;
    }
    for (max=t[i][0], j=0; j<99; j++) {
      if (t[i][j] > max) {
        if (x[i][j] == 0)
          sum++;
        x[i][j] = t[i][j];
        max = t[i][j];
      }
    }
  }

  for (i=0; i<99; i++) {
    if (x[i][98] == 0) {
      x[i][98] = t[i][98];
      sum++;
    }
    for (max=t[i][98], j=98; j>=0; j--) {
      if (t[i][j] > max) {
        if (x[i][j] == 0)
          sum++;
        x[i][j] = t[i][j];
        max = t[i][j];
      }
    }
  }

  for (j=0; j<99; j++) {
    if (x[0][j] == 0) {
      x[0][j] = t[0][j];
      sum++;
    }
    for (max=t[0][j], i=0; i<99; i++) {
      if (t[i][j] > max) {
		if (x[i][j] == 0)
          sum++;
        x[i][j] = t[i][j];
        max = t[i][j];
      }
    }
  }

  for (j=0; j<99; j++) {
    if (x[98][j] == 0) {
      x[98][j] = t[98][j];
      sum++;
    }
    for (max=t[98][j], i=98; i>=0; i--) {
      if (t[i][j] > max) {
        if (x[i][j] == 0)
          sum++;
        x[i][j] = t[i][j];
        max = t[i][j];
      }
    }
  }
  printf ("%d\n", sum);
}

