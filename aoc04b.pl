#!/usr/bin/perl -w

use strict;

my @l;
my $total = 0;

while (<>) {
  s/,/-/;
  @l = split /-/;
  $total += 1
    if ((($l[2] >= $l[0]) and
         ($l[2] <= $l[1])) or
        (($l[3] >= $l[0]) and
         ($l[3] <= $l[1])) or
        (($l[2] <= $l[0]) and
         ($l[3] >= $l[1])));

}
print "$total\n";
