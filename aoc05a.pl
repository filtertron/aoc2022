#!/usr/bin/perl -w

use strict;

my @c1 = ('D', 'T', 'R', 'B', 'J', 'L', 'W', 'G');
my @c2 = ('S', 'W', 'C');
my @c3 = ('R', 'Z', 'T', 'M');
my @c4 = ('D', 'T', 'C', 'H', 'S', 'P', 'V');
my @c5 = ('G', 'P', 'T', 'L', 'D', 'Z');
my @c6 = ('F', 'B', 'R', 'Z', 'J', 'Q', 'C', 'D');
my @c7 = ('S', 'B', 'D', 'J', 'M', 'F', 'T', 'R');
my @c8 = ('L', 'H', 'R', 'B', 'T', 'V', 'M');
my @c9 = ('Q', 'P', 'D', 'S', 'V');

open F, "aoc05.txt" or die;
while (<F>) {
  m/^move/ or next;
  s/move (\S*) from (\S*) to (\S*)/for (my \$i=0; \$i<$1; \$i++) {push \@c$3, pop \@c$2;}/;
  eval;
}
for (my $i=1; $i<=9; $i++) {
  eval "print pop \@c$i;";;
}
print "\n";
