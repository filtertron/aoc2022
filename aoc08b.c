#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>


int main () {

  int  f,
       i,
       j,
       k,
       sum,
       score,
       max = 0;
  char n;
  char t[100][100];

  if ((f = open ("aoc08.txt" , O_RDONLY)) == -1)
    return 1;
  read (f, t, 10000);
  close (f);


  for (n='1'; n<='9'; n++) {
    for (i=1; i<98; i++) {
      for (j=1; j<98; j++) {
        if (t[i][j] == n) {
          sum = 0;
          k = i; 
          do { k--; sum++; } while ((k > 0) && (t[k][j] < n)); 
          score = sum;
          k = i;
          sum = 0;
          do { k++; sum++; } while ((k < 98) && (t[k][j] < n));
          score *= sum;
          sum = 0;
          k = j;
          do { k--; sum++; } while ((k > 0) && (t[i][k] < n));
          score *= sum;
          sum = 0;
          k = j;
          do { k++; sum++; } while ((k < 98) && (t[i][k] < n));
          score *= sum;
          if (score > max)
            max = score;
        }
      }
    }
  }
  printf ("%d\n", max);
}

