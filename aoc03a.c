#include <stdio.h>
#include <string.h>

#define LEN 64


int compare (char *l, int len) {

  int len2, 
      i, 
      j;

  len2 = len / 2;
  for (i = 0; i < len2; i++) {
    for (j = len2; j < len; j++) {
      if (l[i] == l[j]) {
        if ((l[i] >= 'a') && (l[i] <= 'z'))
          l[i] = l[i] - 96;
        if ((l[i] >= 'A') && (l[i] <= 'Z'))
          l[i] = l[i] - 38;
        return (int)l[i];
      }
    }
  }
  printf ("Uh oh\n");
  return 0;
}


int main () {

  char l [LEN];
  FILE *f;
  int  len,
       res,
       sum = 0;

  f = fopen ("aoc03.txt" , "r");
  while (fgets (l, LEN, f)) {
    len = strlen (l);
    res = compare (l, len);
    sum += res;
  }
  printf ("%d\n", sum);
}
