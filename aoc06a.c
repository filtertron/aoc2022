#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

#define LEN 8192

int main () {

  int  f,
       i,
       len;
  char t[LEN];

  if ((f = open ("aoc06.txt" , O_RDONLY)) == -1)
    return 1;
  len = read (f, t, LEN);
  close (f);

  for (i=0; i<len-4; i++) {
    if ((t[i]   != t[i+1]) &&
        (t[i]   != t[i+2]) &&
        (t[i]   != t[i+3]) &&
        (t[i+1] != t[i+2]) &&
        (t[i+1] != t[i+3]) &&
        (t[i+2] != t[i+3])) {
      printf ("%d\n", i+4);
      return 0;
    }
  }
}
